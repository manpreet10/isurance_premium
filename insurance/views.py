from django.template import RequestContext
from django.shortcuts import render
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status
from django.http import HttpResponse, JsonResponse  


class InsuranceCreateApi(APIView):
    def post(self, request, format=None):
        data = request.data
        AGE_FACTOR_DATA = {
            (18, 35): "25",
            (36, 45): "20",
            (46, 50): "15",
            (51, 55): "15",
        }

        if 'age' in data:
            age = int(data['age'])
        else:
            return Response({"status": 400, 'message': 'Please Provide Age', "statusType": "failed"}, status=status.HTTP_400_BAD_REQUEST)

        if age < 18 or age > 55:
            return Response({"status": 400, 'message': 'No Scheme Found', "statusType": "failed"}, status=status.HTTP_400_BAD_REQUEST)

        if 'cover_age' in data:
            cover_age = int(data['cover_age'])
            return Response({"status": 400, 'message': 'Wrong Value for age', "statusType": "failed"}, status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response({"status": 400, 'message': 'Please Provide Cover Age', "statusType": "failed"}, status=status.HTTP_400_BAD_REQUEST)

   
        if age > 18 and age < 55:
            if cover_age <= 65 and data['smoking'] == 'Y':
                cover_age = int(data['cover_age'])
            elif cover_age > 65 and data['smoking'] == 'Y':
                cover_age = 65

            if cover_age <= 75 and data['smoking'] == 'N':
                cover_age = int(data['cover_age'])
            elif cover_age > 75 and data['smoking'] == 'N':
                cover_age = 75

        if data['smoking'] == 'Y':
            maturity_age = 65
        else:
            maturity_age = 75

        for key, value in AGE_FACTOR_DATA.items():
            print(key, value)
            if age >= key[0] and age <= key[1]:
                sum_assured = int(data['annual_income']) * int(value)

        result = {
            'first_name' : data['firstname'],
            'policy_term' : policy_term,
            'sum_assured' :sum_assured,
            'maturity_age':maturity_age
        }
        return Response({"status": 201,'result':result, "statusType": "success"}, status=status.HTTP_201_CREATED)