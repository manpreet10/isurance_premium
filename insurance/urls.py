from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns
from insurance import views

urlpatterns = [
    path('insurance/', views.InsuranceCreateApi.as_view()),
]
